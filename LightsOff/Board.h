#pragma once

#include "Cell.h"
#include "InputValidation.h"
#include <vector>
#include <cstddef>
#include <string>
#include <fstream>

class Board
{
public:
	Board(uint16_t nroflines, uint16_t nrofcolumns);
	Board(std::vector<std::vector<Cell>>& board);
	~Board(); //destructor
	uint16_t getLines() const;
	uint16_t getColumns() const;
	void readBoard(std::string boardfile);
	void writeBoard(int color1, int color2);
	void standardGame();
	void leftrightGame();
	void updownGame();
	void principalDiagGame();
	void secondaryDiagGame();
	void allNearCellsGame();
	void gameMode(uint16_t option);
	bool winningCondition();
	void start();
	std::string fisiere(char linie, char coloana);
	void personalizedMap();

private:
	uint16_t m_lines;
	uint16_t m_columns;
	std::vector<std::vector<Cell>> m_cells;
};