#pragma once
#include <iostream>

class Cell
{
private:
    bool m_isOn;

public:
    Cell(bool isOn);
    ~Cell();
    void setOn();
    void setOff();
    bool getState();        
    bool changeState();   
};