#include "Logger.h"

void InformationMessage()
{
    BOOST_LOG_TRIVIAL(info) << "This is an informational severity message";
}
void ErrorMessage()
{
    BOOST_LOG_TRIVIAL(error) << "This is an error severity message";
}

void WarningMessage()
{
    BOOST_LOG_TRIVIAL(warning) << "This is a warning severity message";
}

