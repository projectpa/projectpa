#pragma once
#include "Board.h"
void isRestartOption(char optiune[50]);
void isOption(char optiune[50]);
void isOptionMod(char optiune[50]);
void isNumber(char  numberline[50], char  numbercolumn[50]);
void isBoardSize(char numberline[50], char numbercolumn[50]);
void isColorOption(char optiune[50]);
void isSecondsNumber(unsigned __int64 seconds);
void isMinutesNumber(unsigned __int64 minutes);
void isMode(char optiune[50]);
void lightsOn(int lights);
void isNumbers(char numberline[50], char numbercolumn[50]);