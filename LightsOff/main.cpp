#include "Cell.h"
#include "Board.h"
#include "Timer.h"
#include "Logger.h"
#include <iostream>
#include <windows.h>
#include <iomanip> 
#include "Timer.h"
#include <chrono>
using namespace std;

int main()
{
    auto start = std::chrono::high_resolution_clock::now();
    uint16_t timestop;
    std::cout << "Welcome to Lights off ! \n";
    std::cout << "The only rule of the game is to turn off all the lights\n \n";
    char moddejoc[10];
    std::cout << "You want to play on the existing maps or you want to create a personalized 5x5 map?\nType 1 for existing maps or 2 for personalized map\n";
    cin >> moddejoc;
    isMode(moddejoc);
    system("cls");
    if (moddejoc[0] == '1')
    {
        char linie[50], coloana[50], restart[50];
        cout << "Please enter the number of lines and columns you want to have the board! \n";
        std::cin >> linie >> coloana;
        isBoardSize(linie, coloana);
        Board board(linie[0] - '0', coloana[0] - '0');
        board.readBoard(board.fisiere(linie[0] - '0', coloana[0] - '0'));
        board.start();
        cout << "\nPlay again?\n";
        cout << "1.Yes\n";
        cout << "2.No\n";
        cin >> restart;
        isRestartOption(restart);
        system("cls");
        while (restart[0] != '2')
        {
            cout << "Please enter the number of lines and columns you want to have the board! \n";
            std::cin >> linie >> coloana;
            isBoardSize(linie, coloana);
            Board board(linie[0] - '0', coloana[0] - '0');
            board.readBoard(board.fisiere(linie[0] - '0', coloana[0] - '0'));
            system("cls");
            board.start();

            cout << "\nPlay again?\n";
            cout << "1.Yes\n";
            cout << "2.No\n";
            cin >> restart;
        }
    }
    else
    {
        char restart[10];
        Board board(5, 5);
        board.readBoard("MatricePersonalizata.txt");
        board.personalizedMap();
        board.start();
        cout << "\nPlay again?\n";
        cout << "1.Yes\n";
        cout << "2.No\n";
        cin >> restart;
        isRestartOption(restart);
        system("cls");
        while (restart[0] != '2')
        {
            Board board(5, 5);
            board.readBoard("MatricePersonalizata.txt");
            board.personalizedMap();
            board.start();

            cout << "\nPlay again?\n";
            cout << "1.Yes\n";
            cout << "2.No\n";
            cin >> restart;
            system("cls");
        }
    }
    auto finish = std::chrono::high_resolution_clock::now();
    timestop = std::chrono::duration_cast<std::chrono::seconds>(finish - start).count();
    int minutes = 0;
    while (timestop > 59)
    {
        minutes++;
        timestop = timestop - 60;
    }

    cout << "The time spent in the game is: " << minutes << " minutes and " << timestop << " seconds \n";
    //InformationMessage();
    //WarningMessage();
    //ErrorMessage();

    return 0;
}