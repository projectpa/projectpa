#include "Timer.h"
void displayClock(unsigned __int64 start, unsigned __int64 stop)
{
    unsigned __int64 hours = 0, minutes = 0, seconds;
    seconds = unsigned __int64((stop - start));
    if (seconds > 59)
    {
        minutes = seconds / 60;
        seconds = seconds - minutes * 60;
    }
    if (minutes > 59)
    {
        hours = minutes / 60;
        minutes = minutes - hours * 60;
    }
    cout << setfill(' ') << setw(55) << " --------------------------\n";
    cout << setfill(' ') << setw(29);
    cout << "| " << setfill('0') << setw(2) << hours << " hrs | ";
    cout << setfill('0') << setw(2) << minutes << " min | ";
    cout << setfill('0') << setw(2) << seconds << " sec |" << endl;
    cout << setfill(' ') << setw(55) << " --------------------------\n";
}