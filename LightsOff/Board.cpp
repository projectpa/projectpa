#include "Board.h"
#include "cell.h"
#include <windows.h> 
#include "Timer.h"
#include "rang.h"
#include <chrono>
using namespace std;
using namespace rang;
unsigned __int64 timestart, timestop;

Board::Board(uint16_t nroflines, uint16_t nrofcolumns)
{
	m_lines = nroflines;
	m_columns = nrofcolumns;
}
Board::Board(std::vector<std::vector<Cell>>& cells)
{
	for (uint16_t line = 0; line < getLines(); ++line)
	{
		std::vector<Cell> elements;
		for (uint16_t column = 0; column < getColumns(); ++column)
		{
			elements.push_back(cells[line][column]);
		}
		m_cells.push_back(elements);
	}
}

Board::~Board()
{

}

uint16_t Board::getLines() const
{
	return m_lines;
}

uint16_t Board::getColumns() const
{
	return m_columns;
}

void Board::readBoard(std::string boardfile)
{
	std::ifstream file_in(boardfile);
	if (file_in.is_open())
	{
		uint16_t element;
		for (uint16_t line = 0; line < getLines(); ++line)
		{
			std::vector<Cell> lines;
			for (uint16_t column = 0; column < getColumns(); ++column)
			{
				file_in >> element;
				lines.push_back((Cell)element);
			}
			m_cells.push_back(lines);
		}
		file_in.close();
	}
	else
	{
		std::cout << "Error with the given file !";
	}
}

void Board::writeBoard(int color1, int color2)
{
	for (uint16_t line = 0; line < getLines(); ++line)
	{
		for (uint16_t column = 0; column < getColumns(); ++column)
		{
			if (m_cells[line][column].getState() == true)
			{
				if (color1 == 1)
					std::cout << rang::fg::blue << line << column << " " << rang::fg::reset;
				else
					if (color1 == 2)
						std::cout << rang::fg::magenta << line << column << " " << rang::fg::reset;
					else
						if (color1 == 3)
							std::cout << rang::fg::cyan << line << column << " " << rang::fg::reset;
						else
							if (color1 == 4)
								std::cout << rang::fg::yellow << line << column << " " << rang::fg::reset;
							else
								if (color1 == 5)
									std::cout << rang::fg::green << line << column << " " << rang::fg::reset;

								else
								{
									std::cout << rang::fg::red << line << column << " " << rang::fg::reset;
								}
			}
			if (m_cells[line][column].getState() == false)
			{
				if (color2 == 1)
					std::cout << rang::fg::blue << line << column << " " << rang::fg::reset;
				else
					if (color2 == 2)
						std::cout << rang::fg::magenta << line << column << " " << rang::fg::reset;
					else
						if (color2 == 3)
							std::cout << rang::fg::cyan << line << column << " " << rang::fg::reset;
						else
							if (color2 == 4)
								std::cout << rang::fg::yellow << line << column << " " << rang::fg::reset;
							else
								if (color2 == 5)
									std::cout << rang::fg::green << line << column << " " << rang::fg::reset;

								else
									if (color2 == 6)
									{
										std::cout << rang::fg::red << line << column << " " << rang::fg::reset;
									}
			}
		}
		std::cout << "\n";
	}
}

bool Board::winningCondition()
{
	for (uint16_t line = 0; line < getLines(); ++line)
	{
		for (uint16_t column = 0; column < getColumns(); ++column)
		{
			if (m_cells[line][column].getState() == true)
			{
				return false;
			}
		}
	}
	return true;
}

void Board::standardGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	isNumber(line, column);
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	m_cells[nrl][nrc].changeState();
	//vecinul din stanga
	if (line[0] - '0' - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) - 1][nrc].changeState();
	}
	//vecinu dreapta
	if (nrl + 1 < m_lines)
	{

		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) + 1][nrc].changeState();
	}

	//vecinul de sus
	if (nrc - 1 > -1)
	{
		m_cells[nrl][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) - 1].changeState();
	}
	//vecinul de jos
	if (nrc+ 1 < m_columns)
	{
		m_cells[nrl][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) + 1].changeState();
	}
}

void Board::updownGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	isNumber(line, column);
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	m_cells[nrl][nrc].changeState();
	//vecinul de sus
	if (nrl - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) - 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) ].changeState();
	}
	//vecinu de jos
	if (nrl + 1 < m_lines)
	{

		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) + 1][nrc].changeState();
	}
}

void Board::leftrightGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	isNumber(line, column);
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	m_cells[nrl][nrc].changeState();
	//vecinul din stanga
	if (nrc - 1 > -1)
	{
		m_cells[nrl][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) - 1].changeState();
	}
	//vecinul din dreapta
	if (nrc + 1 < m_columns)
	{
		m_cells[nrl][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) + 1].changeState();
	}
}

void Board::principalDiagGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	isNumber(line, column);
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	m_cells[nrl][nrc].changeState();
	//diagonala principala (sus)
	if (nrl - 1 > -1 && nrc - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) - 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) - 1].changeState();
	}
	//diagonala principala  (jos)
	if (nrl + 1 < m_lines && nrc + 1 < m_columns)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) + 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) + 1].changeState();
	}
}

void Board::secondaryDiagGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	isNumber(line, column);
	m_cells[nrl][nrc].changeState();
	//diagonala secundara jos
	if (nrl - 1 > -1 && nrc + 1 < m_columns)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) - 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) + 1].changeState();
	}
	//diagonala secundara  sus
	if (nrl + 1 < m_lines && nrc - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) + 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) - 1].changeState();
	}
}

void Board::allNearCellsGame()
{
	char line[50], column[50];
	std::cin >> line >> column;
	isNumber(line, column);
	auto nrl = line[0] - '0';
	auto nrc = column[0] - '0';
	m_cells[nrl][nrc].changeState();
	//vecinul din stanga
	if (nrc - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) ][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc)-1].changeState();
	}
	//vecinul din dreapta
	if (nrc + 1 < m_columns)
	{

		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) ][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc)+1].changeState();
	}
	//vecinul de sus
	if (nrl - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl)-1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) ].changeState();
	}
	//vecinul de jos
	if (nrl + 1 < m_lines)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl)+1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) ].changeState();
	}
	//diagonala principala sus 
	if (nrl - 1 > -1 && nrc - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl)-1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc)-1].changeState();
	}
	//diagonala princiapala  jos
	if (nrl + 1 < m_lines && nrc + 1 < m_columns)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl)+1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) + 1].changeState();
	}
	//diagonala secundara jos 
	if (nrl - 1 > -1 && nrc + 1 < m_columns)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) -1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc) +1].changeState();
	}
	//diagonala secundara  sus
	if (nrl + 1 < m_lines && nrc - 1 > -1)
	{
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(nrl) + 1][static_cast<std::vector<Cell, std::allocator<Cell>>::size_type>(nrc)-1].changeState();
	}
}

void Board::gameMode(uint16_t option)
{
	switch (option)
	{
	case 1:
		Board::standardGame();
		break;
	case 2:
		Board::updownGame();
		break;
	case 3:
		Board::leftrightGame();
		break;
	case 4:
		Board::principalDiagGame();
		break;
	case 5:
		Board::secondaryDiagGame();
		break;
	case 6:
		Board::allNearCellsGame();
		break;
	default:
		std::cout << "Invalid option!\n";
		cin >> option;
		break;
	}
}
void Board::start()
{
	char color1[10], color2[10];
	cout << "Pick the colors you want for the game!\n" << "For the lights which are on \n" << "1.Blue\n2.Magenta\n3.Cyan\n4.Yellow\n5.Green\n6.Red\n";
	cin >> color1;
	isColorOption(color1);
	system("cls");
	cout << "For the lights which are off\n" << "1.Blue\n2.Magenta\n3.Cyan\n4.Yellow\n5.Green\n6.Red\n";
	cin >> color2;
	isColorOption(color2);
	system("cls");
	char option[50];
	std::cout << "Please enter the number of the game mode you want to play: \n1 for standard game \n2 for ";
	std::cout << "for up-down mode \n3 for left-right mode \n4 for principal diagonal \n5 for secondary diagonal \n6 for all near cells \n";
	std::cout << "\n";
	writeBoard(color1[0] - '0', color2[0] - '0');
	std::cout << "\nChoose the mode you want to play! \n";
	std::cin >> option;
	isOption(option);
	char ok[50];
	system("cls");
	cout << "Do you want to play fast game?\n1 Yes\n2 No\n";
	cin >> ok;
	isOptionMod(ok);
	if (ok[0] == '1')
	{
		unsigned __int64 seconds, minutes, hours;
		system("cls");
		cout << "Enter your time! *only numbers*\n\n";
		cout << "Enter your hours:\n";
		cin >> hours;
		cout << "Enter your minutes:\n";
		cin >> minutes;
		isMinutesNumber(minutes);
		cout << "Enter your seconds:\n";
		cin >> seconds;
		isSecondsNumber(seconds);
		auto start = std::chrono::high_resolution_clock::now();
		timestop = hours * 60 * 60 + minutes * 60 + seconds;
		while (!winningCondition())
		{
			system("cls");
			writeBoard(color1[0] - '0', color2[0] - '0');
			std::cout << "Enter the number of line and column you want to switch! \n";
			gameMode(option[0] - '0');
		}
		auto finish = std::chrono::high_resolution_clock::now();
		timestart = std::chrono::duration_cast<std::chrono::seconds>(finish - start).count();
		system("cls");
		writeBoard(color1[0] - '0', color2[0] - '0');
		if (timestart < timestop)
			std::cout << "Congratulations ! You won the game before the time expires!\n ";
		else
			std::cout << "Game Over! You passed the allowed time!\n ";
	}
	else
	{
		timestart = 0;
		auto start = std::chrono::high_resolution_clock::now();
		while (!winningCondition())
		{
			system("cls");
			writeBoard(color1[0] - '0', color2[0] - '0');
			std::cout << "Enter the number of line and column you want to switch! \n";
			gameMode(option[0] - '0');
		}
		auto finish = std::chrono::high_resolution_clock::now();
		timestop = std::chrono::duration_cast<std::chrono::seconds>(finish - start).count();
		system("cls");
		writeBoard(color1[0] - '0', color2[0] - '0');
		std::cout << "Congratulations ! You won the game !\n ";
		cout << " and your time is:\n";
		displayClock(timestart, timestop);
	}
}

std::string Board::fisiere(char linie, char coloana)
{
	std::string document;
	for (char i = '3'; i <= '9'; i++)
		for (char j = '3'; j <= '9'; j++)
			if (linie == i - '0' && coloana == j - '0')
			{
				document = "matrice";
				document = document + i;
				document = document + "x";
				document = document + j;
				document = document + ".txt";
			}
	return document;
}

void Board::personalizedMap()
{
	int lights;
	writeBoard(1, 1);
	cout << " Please enter the number of the lights which you want to on:\n ";
	cin >> lights;
	lightsOn(lights);
	char linie[50], coloana[50];
	for (int i = 0; i < lights; ++i)
	{
		cout << i+1 << ".";
		cin >> linie >> coloana;
		isNumbers(linie, coloana);
		m_cells[static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(linie[0])-'0'][static_cast<std::vector<std::vector<Cell, std::allocator<Cell>>, std::allocator<std::vector<Cell, std::allocator<Cell>>>>::size_type>(coloana[0])-'0'].changeState();
	}
	system("cls");
}