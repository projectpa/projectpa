#include "InputValidation.h"
using namespace std;


void isNumber(char numberline[50], char numbercolumn[50])
{
	while (numberline[0] < '0' || numbercolumn[0] < '0' || numberline[0] >'9' || numbercolumn[0] >'9' || strlen(numberline) != 1 || strlen(numbercolumn) != 1)
	{
		cout << "Invalide numbers!\n";
		cin >> numberline >> numbercolumn;
	}
}
void isOption(char optiune[50])
{
	while (optiune[0] < '1' || optiune[0]>'6' || strlen(optiune) > 1)
	{
		system("cls");
		cout << "Invalide number!\n";
		std::cout << "Please enter the number of the game you want to play: \n1 for standard game \n2 for ";
		std::cout << "for up-down mode \n3 for left-right mode \n4 for principal diagonal \n5 for secondary diagonal \n6 for all near cells \n";
		std::cout << "\n";
		cin >> optiune;
	}
}
void isOptionMod(char optiune[50])
{
	while (optiune[0] < '1' || optiune[0]>'2' || strlen(optiune) > 1)
	{
		system("cls");
		cout << "Invalide number!\n";
		cout << "Do you want to play fast game?\n1 Yes\n2 No\n";
		cin >> optiune;
	}
}
void isRestartOption(char optiune[50])
{
	while (optiune[0] < '1' || optiune[0]>'2' || strlen(optiune) > 1)
	{
		system("cls");
		cout << "Invalide number!\n";
		cout << "\nPlay again?\n";
		cout << "1.Yes\n";
		cout << "2.No\n";
		cin >> optiune;
	}
}
void isBoardSize(char numberline[50], char numbercolumn[50])
{
	while (numberline[0] < '1' || numbercolumn[0] < '1' || numberline[0] >'9' || numbercolumn[0] >'9' || strlen(numberline) != 1 || strlen(numbercolumn) != 1)
	{
		system("cls");
		std::cout << "Invalid number of line and columns. Please enter the correct dimension! !\n";
		std::cin >> numberline >> numbercolumn;
	}
	system("cls");
}



void isColorOption(char optiune[50])
{
	while (optiune[0] < '1' || optiune[0]>'6' || strlen(optiune) > 1)
	{
		system("cls");
		cout << "Alege culoarea cu care vrei sa joc:\n" << "1.Blue\n2.Magenta\n3.Cyan\n4.Yellow\n5.Green\n6.Red\n";
		cin >> optiune;
	}
}

void isSecondsNumber(unsigned __int64 seconds)
{
	while ( seconds > 59)
	{
		cout << "Alege un numar de secunde < 60 sau valid !\n" ;
		cin >> seconds;
	}
}

void isMinutesNumber(unsigned __int64 minutes)
{
	while (minutes > 59)
	{
		cout << "Alege un numar valid de minute ( < 60 ) sau format doar din cifre!\n";
		cin >> minutes;
	}
}

void isMode(char optiune[50])
{
	while (optiune[0] < '1' || optiune[0]>'2' || strlen(optiune) > 1)
	{
		system("cls");
		std::cout << "Welcome to Lights off ! \n";
		std::cout << "The only rule of the game is to turn off all the lights\n \n";
		cout << "Type 1 for existing maps or 2 for personalized map\n";
		cin >> optiune;
	}
}

void lightsOn(int lights)
{
	if (lights > 25 )
	{
		cout << "The number of lights off exceeds the number of cells! Please enter e valid number !\n";
		cin >> lights;
	}
}
void isNumbers(char numberline[50], char numbercolumn[50])
{
	while (numberline[0] < '0' || numbercolumn[0] < '0' || numberline[0] >'4' || numbercolumn[0] >'4' || strlen(numberline) != 1 || strlen(numbercolumn) != 1)
	{
		cout << "Invalid numbers!\n";
		cin >> numberline >> numbercolumn;
	}
}