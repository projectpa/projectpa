#include "cell.h"

Cell::Cell(bool isOn)
{
	m_isOn = isOn;
}

Cell::~Cell()
{

}

bool Cell::getState()
{
	return m_isOn;
}

void Cell::setOn()
{
	m_isOn = true;
}

void Cell::setOff()
{
	m_isOn = false;
}

bool Cell::changeState()
{
	if (m_isOn == true)
		setOff();
	else
		setOn();
	return m_isOn;
}



